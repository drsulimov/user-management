package com.cgi.usermanagement.api;

import lombok.Data;

@Data
public class PersonAuthDto {
    private String email;
    private String password;

    public PersonAuthDto(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
