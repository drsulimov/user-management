package com.cgi.usermanagement.api;

import lombok.Data;

import java.util.Set;

@Data
public class PersonInformationDto {
    private PersonDto person;
    private AddressDto address;
    private Set<GroupDto> groups;
    private Set<RoleDto> roles;
}
