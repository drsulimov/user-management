package com.cgi.usermanagement.api;

import lombok.Data;

import java.time.LocalDate;

@Data
public class GroupDto {
    private Long id;
    private String name;
    private String description;
    private LocalDate expirationDate;
}
