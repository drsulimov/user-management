package com.cgi.usermanagement.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

@Data
public class RoleDto implements GrantedAuthority {
    private Long id;
    private String roleType;
    private String description;

    @Override
    @JsonIgnore
    public String getAuthority() {
        return roleType;
    }
}
