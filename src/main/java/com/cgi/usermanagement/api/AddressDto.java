package com.cgi.usermanagement.api;

import lombok.Data;

@Data
public class AddressDto {
    private Long id;
    private String city;
    private String country;
    private String street;
    private String postcode;
}
