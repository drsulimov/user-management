package com.cgi.usermanagement.mappers;

import com.cgi.usermanagement.entities.Address;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddressMapper implements RowMapper<Address> {
    @Override
    public Address mapRow(ResultSet resultSet, int i) throws SQLException {
        Address address = new Address();
        address.setId(resultSet.getLong("id"));
        address.setCity(resultSet.getString("city"));
        address.setCountry(resultSet.getString("country"));
        address.setStreet(resultSet.getString("street"));
        address.setPostcode(resultSet.getString("postcode"));
        return address;
    }
}
