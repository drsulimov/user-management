package com.cgi.usermanagement.mappers;

import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface BeanMapping {
    <T> List<T> mapTo(Collection<?> objects, Class<T> mapToClass);

    <T> Page<T> mapTo(Page<?> objects, Class<T> mapToClass);

    <T> Set<T> mapToSet(Collection<?> objects, Class<T> mapToClass);

    <T> Optional<T> mapToOptional(Object u, Class<T> mapToClass);

    <T> T mapTo(Object u, Class<T> mapToClass);
}
