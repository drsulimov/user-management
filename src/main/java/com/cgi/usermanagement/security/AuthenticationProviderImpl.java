package com.cgi.usermanagement.security;

import com.cgi.usermanagement.api.PersonAuthDto;
import com.cgi.usermanagement.services.UserAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {
    @Autowired
    private UserAuthenticationService userAuthenticationService;

    @Autowired
    @Qualifier("proprietaryUserDetailsService")
    private UserDetailsService userDetailsService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        PersonAuthDto personAuthDto = new PersonAuthDto(username, password);

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (!userAuthenticationService.authenticate(personAuthDto, userDetails)) {
            throw new BadCredentialsException("Provided password is not valid");
        }
        return new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
