package com.cgi.usermanagement.repositories;

import com.cgi.usermanagement.entities.Person;
import com.cgi.usermanagement.mappers.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Person getPersonById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM person p WHERE p.id = ?",
                new Object[]{id},
                new PersonMapper());
    }

    public Person getPersonByEmail(String email) {
        return jdbcTemplate.queryForObject("SELECT * FROM person p WHERE p.email = ?",
                new Object[]{email},
                new PersonMapper());
    }
}
