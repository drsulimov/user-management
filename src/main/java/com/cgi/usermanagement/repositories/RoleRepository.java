package com.cgi.usermanagement.repositories;

import com.cgi.usermanagement.entities.Role;
import com.cgi.usermanagement.mappers.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class RoleRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public RoleRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Set<Role> getAllRolesByPersonId(Long id) {
        return new HashSet<>(jdbcTemplate.query("SELECT r.* " +
                        "FROM role r " +
                        "JOIN group_role gr ON r.id = gr.role_id " +
                        "JOIN \"Group\" g ON gr.group_id = g.id " +
                        "JOIN person_group pg ON g.id = pg.group_id " +
                        "JOIN person p ON pg.person_id = p.id " +
                        "WHERE p.id = ?",
                new Object[]{id},
                new RoleMapper()));
    }
}
