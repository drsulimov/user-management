package com.cgi.usermanagement.repositories;

import com.cgi.usermanagement.entities.*;
import com.cgi.usermanagement.mappers.AddressMapper;
import com.cgi.usermanagement.mappers.GroupMapper;
import com.cgi.usermanagement.mappers.PersonMapper;
import com.cgi.usermanagement.mappers.RoleMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class PersonInformationRepository {
    private JdbcTemplate jdbcTemplate;

    public PersonInformationRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private Person getPersonById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * " +
                        "FROM person p " +
                        "WHERE p.id = ?",
                new Object[]{id},
                new PersonMapper());
    }

    private Address getAddressByPersonId(Long id) {
        return jdbcTemplate.queryForObject("SELECT * " +
                        "FROM address a " +
                        "JOIN person p ON a.id = p.address_id " +
                        "WHERE p.id = ?",
                new Object[]{id},
                new AddressMapper());
    }

    private Set<Group> getAllGroupsByPersonId(Long id) {
        return new HashSet<>(jdbcTemplate.query("SELECT * " +
                        "FROM \"Group\" g " +
                        "JOIN person_group pg ON g.id = pg.group_id " +
                        "JOIN person p ON pg.person_id = p.id " +
                        "WHERE p.id = ?",
                new Object[]{id},
                new GroupMapper()));
    }

    private Set<Role> getAllRolesByPersonId(Long id) {
        return new HashSet<>(jdbcTemplate.query("SELECT r.* " +
                        "FROM role r " +
                        "JOIN group_role gr ON r.id = gr.role_id " +
                        "JOIN \"Group\" g ON gr.group_id = g.id " +
                        "JOIN person_group pg ON g.id = pg.group_id " +
                        "JOIN person p ON pg.person_id = p.id " +
                        "WHERE p.id = ?",
                new Object[]{id},
                new RoleMapper()));
    }

    public PersonInformation getPersonInformationByPersonId(Long id) {
        PersonInformation personInformation = new PersonInformation();
        personInformation.setPerson(getPersonById(id));
        personInformation.setAddress(getAddressByPersonId(id));
        personInformation.setGroups(getAllGroupsByPersonId(id));
        personInformation.setRoles(getAllRolesByPersonId(id));
        return personInformation;
    }
}
