package com.cgi.usermanagement.services;

import com.cgi.usermanagement.api.PersonDto;
import com.cgi.usermanagement.entities.Person;
import com.cgi.usermanagement.mappers.BeanMapping;
import com.cgi.usermanagement.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("proprietaryUserDetailsService")
public class PersonServiceImpl implements PersonService, UserDetailsService {
    private PersonRepository personRepository;
    private BeanMapping beanMapping;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository, BeanMapping beanMapping) {
        this.personRepository = personRepository;
        this.beanMapping = beanMapping;
    }

    @Override
    public PersonDto getPersonById(Long id) {
        Person person = personRepository.getPersonById(id);
        return beanMapping.mapTo(person, PersonDto.class);
    }

    @Override
    public PersonDto getPersonByEmail(String email) {
        Person person = personRepository.getPersonByEmail(email);
        return beanMapping.mapTo(person, PersonDto.class);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Person person = personRepository.getPersonByEmail(s);
        return beanMapping.mapTo(person, PersonDto.class);
    }
}
