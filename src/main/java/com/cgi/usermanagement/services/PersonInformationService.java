package com.cgi.usermanagement.services;

import com.cgi.usermanagement.api.PersonInformationDto;

public interface PersonInformationService {
    PersonInformationDto getPersonInformationByPersonId(Long id);
}
