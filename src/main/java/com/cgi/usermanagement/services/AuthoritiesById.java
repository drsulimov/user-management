package com.cgi.usermanagement.services;

import com.cgi.usermanagement.api.RoleDto;
import com.cgi.usermanagement.entities.Role;
import com.cgi.usermanagement.mappers.BeanMapping;
import com.cgi.usermanagement.repositories.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class AuthoritiesById {
    private static RoleRepository roleRepository;
    private static BeanMapping beanMapping;

    public AuthoritiesById(RoleRepository roleRepository, BeanMapping beanMapping) {
        AuthoritiesById.roleRepository = roleRepository;
        AuthoritiesById.beanMapping = beanMapping;
    }

    public static Set<RoleDto> getAllRolesByPersonId(Long id) {
        Set<Role> roles = roleRepository.getAllRolesByPersonId(id);
        return new HashSet<>(beanMapping.mapTo(roles, RoleDto.class));
    }
}
