package com.cgi.usermanagement.services;

import com.cgi.usermanagement.api.PersonInformationDto;
import com.cgi.usermanagement.entities.PersonInformation;
import com.cgi.usermanagement.mappers.BeanMapping;
import com.cgi.usermanagement.repositories.PersonInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonInformationServiceImpl implements PersonInformationService {
    private PersonInformationRepository personInformationRepository;
    private BeanMapping beanMapping;

    @Autowired
    public PersonInformationServiceImpl(PersonInformationRepository personInformationRepository, BeanMapping beanMapping) {
        this.personInformationRepository = personInformationRepository;
        this.beanMapping = beanMapping;
    }

    @Override
    public PersonInformationDto getPersonInformationByPersonId(Long id) {
        PersonInformation personInformation = personInformationRepository.getPersonInformationByPersonId(id);
        return beanMapping.mapTo(personInformation, PersonInformationDto.class);
    }
}
