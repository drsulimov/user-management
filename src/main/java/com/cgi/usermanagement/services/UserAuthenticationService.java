package com.cgi.usermanagement.services;

import com.cgi.usermanagement.api.PersonAuthDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserAuthenticationService {
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserAuthenticationService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public boolean authenticate(PersonAuthDto personAuthDto, UserDetails userDetails) {
        return passwordEncoder.matches(personAuthDto.getPassword(), userDetails.getPassword());
    }
}
