package com.cgi.usermanagement.services;

import com.cgi.usermanagement.api.RoleDto;
import com.cgi.usermanagement.entities.Role;
import com.cgi.usermanagement.mappers.BeanMapping;
import com.cgi.usermanagement.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {
    private RoleRepository roleRepository;
    private BeanMapping beanMapping;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository, BeanMapping beanMapping) {
        this.roleRepository = roleRepository;
        this.beanMapping = beanMapping;
    }

    @Override
    public Set<RoleDto> getAllRolesByPersonId(Long id) {
        Set<Role> roles = roleRepository.getAllRolesByPersonId(id);
        return new HashSet<>(beanMapping.mapTo(roles, RoleDto.class));
    }
}
