package com.cgi.usermanagement.services;

import com.cgi.usermanagement.api.PersonDto;

public interface PersonService {
    PersonDto getPersonById(Long id);

    PersonDto getPersonByEmail(String email);
}
