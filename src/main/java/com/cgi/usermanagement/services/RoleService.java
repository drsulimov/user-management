package com.cgi.usermanagement.services;

import com.cgi.usermanagement.api.RoleDto;

import java.util.Set;

public interface RoleService {
    Set<RoleDto> getAllRolesByPersonId(Long id);
}
