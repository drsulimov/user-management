package com.cgi.usermanagement.entities;

import lombok.Data;

@Data
public class Address {
    private Long id;
    private String city;
    private String country;
    private String street;
    private String postcode;
}
