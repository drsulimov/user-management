package com.cgi.usermanagement.entities;

import lombok.Data;

@Data
public class Role {
    private Long id;
    private String roleType;
    private String description;
}
