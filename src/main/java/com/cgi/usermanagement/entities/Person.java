package com.cgi.usermanagement.entities;

import lombok.Data;

@Data
public class Person {
    private Long id;
    private String name;
    private String email;
    private String password;
    private Double salary;
}
