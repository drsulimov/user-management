package com.cgi.usermanagement.entities;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Group {
    private Long id;
    private String name;
    private String description;
    private LocalDate expirationDate;
}
