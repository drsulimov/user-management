package com.cgi.usermanagement.entities;

import lombok.Data;

import java.util.Set;

@Data
public class PersonInformation {
    private Person person;
    private Address address;
    private Set<Group> groups;
    private Set<Role> roles;
}
