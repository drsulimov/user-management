package com.cgi.usermanagement.controllers;

import com.cgi.usermanagement.api.RoleDto;
import com.cgi.usermanagement.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/api/v1/basic-information/roles")
public class RoleController {
    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @RequestMapping(params = "person_id", method = RequestMethod.GET)
    public ResponseEntity<Set<RoleDto>> getAllRolesByPersonId(@RequestParam Long person_id) {
        return new ResponseEntity<>(roleService.getAllRolesByPersonId(person_id), HttpStatus.OK);
    }
}
