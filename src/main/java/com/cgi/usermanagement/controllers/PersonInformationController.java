package com.cgi.usermanagement.controllers;

import com.cgi.usermanagement.api.PersonInformationDto;
import com.cgi.usermanagement.services.PersonInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/extended-information/persons/")
public class PersonInformationController {
    private PersonInformationService personInformationService;

    @Autowired
    public PersonInformationController(PersonInformationService personInformationService) {
        this.personInformationService = personInformationService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonInformationDto> getPersonInformationByPersonId(@PathVariable Long id) {
        return new ResponseEntity<>(personInformationService.getPersonInformationByPersonId(id), HttpStatus.OK);
    }
}
