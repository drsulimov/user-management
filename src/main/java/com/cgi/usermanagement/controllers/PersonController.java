package com.cgi.usermanagement.controllers;

import com.cgi.usermanagement.api.PersonDto;
import com.cgi.usermanagement.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/basic-information/persons")
public class PersonController {
    private PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDto> getPersonById(@PathVariable Long id) {
        return new ResponseEntity<>(personService.getPersonById(id), HttpStatus.OK);
    }

    @RequestMapping(params = "email", method = RequestMethod.GET)
    public ResponseEntity<PersonDto> getPersonByEmail(@RequestParam String email) {
        return new ResponseEntity<>(personService.getPersonByEmail(email), HttpStatus.OK);
    }
}
